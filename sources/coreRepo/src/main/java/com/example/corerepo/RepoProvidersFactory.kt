package com.example.corerepo

import com.example.core_api.database.DatabaseProvider
import com.example.core_api.mediator.AppProvider
import com.example.repo.RepoProvider
import com.example.repo_impl.di.DaggerRepoComponent


object CoreRepoFactory {

        fun createRepo(appProvider: AppProvider, databaseProvider: DatabaseProvider): RepoProvider {
            return DaggerRepoComponent.builder().appProvider(appProvider).databaseProvider(databaseProvider).build()
        }
    }
