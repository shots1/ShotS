package com.example.core_api.database

import com.example.core_api.database.dao.FiltersDao
import com.example.core_api.database.dao.MarkserDao
import com.example.core_api.database.dao.UserLocationDao

interface ShotSDatabaseContract {

     fun userLocation(): UserLocationDao
     fun marker(): MarkserDao
     fun filters(): FiltersDao
}