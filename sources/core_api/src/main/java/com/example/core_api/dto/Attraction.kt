package com.example.core_api.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(
    tableName = "attractions"
)
data class Attraction(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name:String,
    val latitude: Double,
    val longitude: Double,
    val img: ByteArray?,
    @ColumnInfo(defaultValue = "нет информации")
    val description:String,
    val startWork:Long?,
    val endWork:Long?,
    @ColumnInfo(defaultValue = "0.0")
    val rating: Float?
)

