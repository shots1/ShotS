package com.example.core_api.dto



data class LoginResponse(
    val user: User
)