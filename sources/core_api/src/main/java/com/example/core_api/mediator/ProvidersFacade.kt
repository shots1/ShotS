package com.example.core_api.mediator

import com.example.core_api.database.DatabaseProvider
import com.example.core_api.network.NetworkProvider

interface ProvidersFacade : MediatorsProvider, DatabaseProvider, AppProvider, NetworkProvider