package com.example.core_api.dto

data class TokenResponse(
    val access_token: String,
    val refresh_token: String
)