package com.example.core_api

import javax.inject.Qualifier
import javax.inject.Scope


@Scope
annotation class LoginActivityScope
@Scope
annotation class FragmentScope
@Scope
annotation class HomeScope
@Scope
annotation class MainActivityScope

@Scope
annotation class LocationServiceScope

@Scope
annotation class SplashActivityScope
@Qualifier
annotation class SplashAcitivityContext
@Qualifier
annotation class MainActivityContext
@Qualifier
annotation class ApplicationContext
//@Named
//annotation class ApplicationContext





