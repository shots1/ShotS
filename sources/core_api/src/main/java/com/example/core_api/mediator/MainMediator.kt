package com.example.core_api.mediator

import android.content.Context

interface MainMediator {

    fun openMainScreen(context: Context)
}