package com.example.core_api.database.conventors

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.TypeConverter
import java.io.ByteArrayOutputStream

class DBConvertor {
    class PrimitiveTypeConvertor {
        @TypeConverter
        fun booleanToInt(param: Boolean): Int = if (param) 1 else 0

        @TypeConverter
        fun intToBoolean(param: Int): Boolean = param == 1
    }

    class ClassTypeConventor{
        @TypeConverter
        fun toBitmap(bytes: ByteArray): Bitmap {
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        }

        @TypeConverter
        fun fromBitmap(bmp: Bitmap): ByteArray {
            val outputStream = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            return outputStream.toByteArray()
        }

    }
}