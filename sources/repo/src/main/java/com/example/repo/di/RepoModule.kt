package com.example.repo.di

import com.example.repo.filter.FiltersRepository
import com.example.repo.filter.FiltersRepositoryImpl
import com.example.repo.gps.GpsDataSource
import com.example.repo.gps.GpsDataSourceImpl
import com.example.repo.hardware.GpsRepository
import com.example.repo.hardware.GpsRepositoryImpl
import com.example.repo.local.UserPreferences
import com.example.repo.local.UserPreferencesImpl
import com.example.repo.location.MarkerRepositoryImpl
import com.example.repo.location.UserLocationRepository
import com.example.repo.markers.MarkerRepository
import com.example.repo.markers.UserLocationRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepoModule {


    @Suppress("FunctionName")
    @Binds
    fun bindsMarkserRepository_to_MarkerRepositoryImpl(userPointRepositoryImpl: MarkerRepositoryImpl): MarkerRepository


    @Suppress("FunctionName")
    @Binds
    fun bindsFilterRepository_to_FilterRepositoryImpl(filtersRepositoryImpl: FiltersRepositoryImpl): FiltersRepository


    @Suppress("FunctionName")
    @Binds
    fun bindsGpsDataSource_to_GpsDataSourceImpl(gpsDataSourceImpl: GpsDataSourceImpl): GpsDataSource


    @Suppress("FunctionName")
    @Binds
    fun bindsGpsRepo_to_GpsRepoImpl(gpsRepositoryImpl: GpsRepositoryImpl): GpsRepository


    @Suppress("FunctionName")
    @Binds
    fun bindsUserLocationRepository_to_UserLocationRepositoryImpl(userLocationRepositoryImpl: UserLocationRepositoryImpl): UserLocationRepository

    @Suppress("FunctionName")
    @Binds
    fun bindsUserPref_to_UserPref_Impl(userPreferencesImpl: UserPreferencesImpl): UserPreferences


}