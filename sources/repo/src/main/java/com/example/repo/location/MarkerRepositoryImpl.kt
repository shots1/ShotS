package com.example.repo.location


import com.example.core_api.database.ShotSDatabaseContract
import com.example.core_api.dto.Filter
import com.example.core_api.dto.MarkerPoint
import com.example.repo.BaseRepositoryDataBase
import com.example.repo.markers.MarkerRepository
import com.example.utils.ResponseDataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class MarkerRepositoryImpl @Inject constructor(
    private val databaseSource: ShotSDatabaseContract
): MarkerRepository, BaseRepositoryDataBase() {


    override suspend fun getAllMarkersSuitRating(rating: Double): Flow<ResponseDataBase<MarkerPoint>> {
      return  databaseSource.marker().getAllMarkerSuitRating(rating).transform {
            doWork(it, this)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getAllMarksers(): Flow<ResponseDataBase<MarkerPoint>> {
        return  databaseSource.marker().getAllMarksers().transform {
            doWork(it, this)
        }.flowOn(Dispatchers.IO)
    }

    override fun getAllMarksersWithFilter(filter: Filter): Flow<ResponseDataBase<MarkerPoint>> {
        TODO("Not yet implemented")
    }

    override suspend fun getAllMarkersByType(type: Int): Flow<ResponseDataBase<MarkerPoint>> {
        return  databaseSource.marker().getAllMarkersByType(type).transform {
            doWork(it, this)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getAllMarkersByTypeSuitRating(
        type: Int,
        rating: Double,
    ): Flow<ResponseDataBase<MarkerPoint>> {

        return  databaseSource.marker().getAllMarkersByTypeSuitRating(type ,rating).transform {
            doWork(it, this)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun insert(item: MarkerPoint) {
        databaseSource.marker().insertOrUpdate(item)
    }

    override suspend fun insertList(item: List<MarkerPoint>) {
        databaseSource.marker().insertOrUpdate(item)
    }

    override suspend fun delete(list: List<MarkerPoint>) {
        databaseSource.marker().delete(list)
    }
}

