package com.example.repo.local

import kotlinx.coroutines.flow.Flow

interface UserPreferences {
    val accessToken: Flow<String?>

    val refreshToken: Flow<String?>

    suspend fun saveAccessTokens(accessToken: String?, refreshToken: String?)

    suspend fun clear()
}
