package com.example.repo.local.crypto.Biometry

data class EncryptedEntity(
    val ciphertext: ByteArray,
    val iv: ByteArray
)