package com.example.repo.filter


import com.example.core_api.database.ShotSDatabaseContract
import com.example.core_api.dto.Filter
import com.example.repo.BaseRepositoryDataBase
import com.example.utils.ResponseDataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class FiltersRepositoryImpl @Inject constructor(
    private val databaseSource: ShotSDatabaseContract
): FiltersRepository, BaseRepositoryDataBase() {
    override  fun getFilters(): Flow<ResponseDataBase<Filter>> {
      return  databaseSource.filters().getFilter().transform {
          doWorkNotList(it, this)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun insert(item: Filter) {
        databaseSource.filters().insertOrUpdate(item)
    }

    override suspend fun insertOrIgnore(item: Filter) {
        databaseSource.filters().insertOrIgnore(item)
    }

    override suspend fun delete() {
        databaseSource.filters().nukeTable()
    }
}