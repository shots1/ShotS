package com.example.repo.filter

import com.example.core_api.dto.Filter
import com.example.utils.ResponseDataBase
import kotlinx.coroutines.flow.Flow

interface FiltersRepository {

     fun getFilters(): Flow<ResponseDataBase<Filter>>

    suspend fun insert(item: Filter)

    suspend fun insertOrIgnore(item: Filter)

    suspend fun delete()
}