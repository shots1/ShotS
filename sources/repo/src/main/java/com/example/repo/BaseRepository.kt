package com.example.repo


import com.example.utils.ResponseDataBase
import kotlinx.coroutines.flow.FlowCollector



abstract class BaseRepositoryDataBase {
    protected suspend fun <T> doWork(value: List<T>, collector: FlowCollector<ResponseDataBase<T>>) {
        try {
            if (value.isEmpty()) {
                collector.emit(ResponseDataBase.Empty)
            } else {
                collector.emit(ResponseDataBase.Success(value))
            }
        } catch (e: Exception) {
            collector.emit(ResponseDataBase.Failure(e))
        }
    }

    protected suspend fun <T> doWorkNotList(value: T,collector: FlowCollector<ResponseDataBase<T>>) {
        try {
            if (value == null ) {
                collector.emit(ResponseDataBase.Empty)
            } else {
                collector.emit(ResponseDataBase.SuccessNotList(value))
            }
        } catch (e: Exception) {
            collector.emit(ResponseDataBase.Failure(e))
        }
    }
}