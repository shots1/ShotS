package com.example.repo.markers

import com.example.core_api.database.ShotSDatabaseContract
import com.example.core_api.dto.UserLocation
import com.example.repo.BaseRepositoryDataBase
import com.example.repo.location.UserLocationRepository
import com.example.utils.ResponseDataBase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class UserLocationRepositoryImpl @Inject constructor(
    private val databaseSource: ShotSDatabaseContract
): UserLocationRepository, BaseRepositoryDataBase() {
    override suspend fun getLastLocation(): Flow<ResponseDataBase<UserLocation>> {
        return  databaseSource.userLocation().getLastLocation().transform {
            doWorkNotList(it,this)
        }
    }

    override suspend fun getAllUserLocations(): Flow<ResponseDataBase<UserLocation>> {
       return databaseSource.userLocation().getAllUserLocation().transform {
           doWork(it,this)
       }
    }

    override suspend fun insert(item: UserLocation) {
        databaseSource.userLocation().insertOrUpdate(item)
    }

    override suspend fun delete(list: List<UserLocation>) {
        databaseSource.userLocation().delete(list)
    }
}