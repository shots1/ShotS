package com.example.repo.markers


import com.example.core_api.dto.Filter
import com.example.core_api.dto.MarkerPoint
import com.example.utils.ResponseDataBase

import kotlinx.coroutines.flow.Flow

interface MainRepo <T>{
    suspend fun insert(item: T)

    suspend fun insertList(item:List<T>)

    suspend fun delete(list: List<T>)
}

interface MarkerRepository : MainRepo<MarkerPoint> {

    suspend fun getAllMarkersSuitRating(rating:Double): Flow<ResponseDataBase<MarkerPoint>>

    suspend fun getAllMarksers(): Flow<ResponseDataBase<MarkerPoint>>

     fun getAllMarksersWithFilter(filter: Filter): Flow<ResponseDataBase<MarkerPoint>>

    suspend fun getAllMarkersByType(type:Int): Flow<ResponseDataBase<MarkerPoint>>

    suspend fun getAllMarkersByTypeSuitRating(type:Int,rating:Double): Flow<ResponseDataBase<MarkerPoint>>
}
