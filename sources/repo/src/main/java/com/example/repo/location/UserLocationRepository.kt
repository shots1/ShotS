package com.example.repo.location


import com.example.core_api.dto.UserLocation
import com.example.utils.ResponseDataBase
import kotlinx.coroutines.flow.Flow

interface UserLocationRepository {

    suspend fun getLastLocation(): Flow<ResponseDataBase<UserLocation>>

    suspend fun getAllUserLocations(): Flow<ResponseDataBase<UserLocation>>

    suspend fun insert(item: UserLocation)

    suspend fun delete(list: List<UserLocation>)

}