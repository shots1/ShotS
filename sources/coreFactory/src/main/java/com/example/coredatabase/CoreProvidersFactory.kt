package com.example.coredatabase

import com.example.core_api.database.DatabaseProvider
import com.example.core_api.mediator.AppProvider
import com.example.core_api.network.NetworkProvider
import com.example.core_impl.DaggerDatabaseComponent
import com.example.core_impl.network.DaggerNetworkComponent

object CoreProvidersFactory {

    fun createDatabaseBuilder(appProvider: AppProvider): DatabaseProvider {
        return DaggerDatabaseComponent.builder().appProvider(appProvider).build()
    }
    fun createRetrofit(): NetworkProvider {
        return DaggerNetworkComponent.builder().build()
    }
}
