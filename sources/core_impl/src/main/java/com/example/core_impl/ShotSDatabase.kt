package com.example.core_impl

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import com.example.core_api.database.ShotSDatabaseContract
import com.example.core_api.database.conventors.DBConvertor
import com.example.core_api.dto.Filter
import com.example.core_api.dto.MarkerPoint
import com.example.core_api.dto.UserLocation


@Database(
    version = 1,
    entities = [
        UserLocation::class,
        MarkerPoint::class,
        Filter::class
    ],
    autoMigrations = [
    ],
    exportSchema = true
)
@TypeConverters(DBConvertor.PrimitiveTypeConvertor::class)
abstract class ShotSDatabase : RoomDatabase(), ShotSDatabaseContract {

    companion object {
        val migrations: Array<Migration> = arrayOf(
        )
        const val DATABASE = "gulyat"
    }
}