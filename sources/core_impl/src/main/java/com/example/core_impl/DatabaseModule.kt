package com.example.core_impl

import android.content.Context
import androidx.room.Room
import com.example.core_api.database.ShotSDatabaseContract
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

private const val ShotS_DATABASE_NAME = "ShotS"

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideShotSDatabase(context: Context): ShotSDatabaseContract {
        return Room.databaseBuilder(
            context,
            ShotSDatabase::class.java, ShotS_DATABASE_NAME
        ).build()
    }
}