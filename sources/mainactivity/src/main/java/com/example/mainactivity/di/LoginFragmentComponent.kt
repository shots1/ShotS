package com.example.mainactivity.di


import com.example.core_api.mediator.ProvidersFacade
import com.example.mainactivity.login.LoginFragment
import com.example.repo.di.RepoModule
import dagger.Component


@Component(
    dependencies = [ProvidersFacade::class],
    modules = [LoginModule::class,RepoModule::class]
)
interface LoginFragmentComponent {
    fun inject(fragment: LoginFragment)
    companion object {

        fun create(providersFacade: ProvidersFacade): LoginFragmentComponent {
            return DaggerLoginFragmentComponent.builder().providersFacade(providersFacade).build()
        }
    }
}
