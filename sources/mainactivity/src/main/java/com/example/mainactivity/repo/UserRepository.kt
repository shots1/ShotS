package com.example.mainactivity.repo

import com.example.mainactivity.network.UserApi
import javax.inject.Inject

class UserRepository
@Inject constructor(
    private val api: UserApi
) : BaseRepository(api) {

    suspend fun getUser() = safeApiCall { api.getUser() }
}