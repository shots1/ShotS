package com.example.mainactivity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.database.sqlite.SQLiteException
import android.graphics.Bitmap
import android.util.Log
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.core_api.dto.MarkerPoint
import com.example.core_api.dto.UserLocation
import com.example.repo.hardware.GpsRepository
import com.example.repo.local.UserPreferences
import com.example.repo.location.UserLocationRepository
import com.example.utils.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import javax.inject.Inject


class MainViewModel(
    private val userPreferences: UserPreferences,
    private val gpsRepository: GpsRepository,
    private val markerManager: MarkerManager,
    private val userLocationRepository: UserLocationRepository,
) : ViewModel() {
    private val _sharedStateFlowError = MutableSharedFlow<ErrorApp<Any?>>(replay = 0,
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val sharedStateFlowError = _sharedStateFlowError.asSharedFlow()
    private val coroutineException = CoroutineExceptionHandler { coroutineContext, throwable ->
        viewModelScope.launch(Dispatchers.Main) {
            _sharedStateFlowError.emit(ErrorApp.FailureUnknown(throwable.toString()))
            Log.d("MainViewModel", throwable.toString())
        }
    } + CoroutineName("CatsCoroutine")
    private val _responseDataBaseStateFlow: MutableStateFlow<DataToMain<MarkerPoint>> =
        MutableStateFlow(DataToMain.Non)
    val responseDataBaseStateFlow: StateFlow<DataToMain<MarkerPoint>> =
        _responseDataBaseStateFlow.asStateFlow()

    private val locationFlowToMain: StateFlow<ResponseHome> =
        markerManager.locationFlow.asStateFlow()

    private val sharedFlowFromFilter = markerManager.markerPointFromFilters.asSharedFlow()

    val markerFlowToMain = markerManager.markerFlowToMain.asSharedFlow()

    init {
        Log.d("MainViewModel", "Attraction")
        viewModelScope.launch(Dispatchers.IO + coroutineException) {

            locationFlowToMain.collect {
                println("initFlowLocationHome      : I'm working in thread ${Thread.currentThread().name}")
                when (it) {
                    ResponseHome.Attraction -> {
                        Log.d("MainViewModel", "Attraction")
                        getAttraction()
                    }
                    ResponseHome.PhotoZone -> {
                        getPhotoZone()
                    }
                    ResponseHome.UserPoint -> {
                        getUserPoint()
                    }
                    ResponseHome.Loading -> {
                        clearDataFromMap()
                    }
                }
            }
        }
        viewModelScope.launch {
            sharedFlowFromFilter.collect { markers ->
                val sortedMap = markers.groupBy { it.type }
                println("данные пришли от фильтра, очистка ${Thread.currentThread().name}")
                clearDataFromMap()
                _responseDataBaseStateFlow.emit(DataToMain.Success(sortedMap))
            }
        }
        getLastLocation()
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    fun mockDatabase(context: Context) {

        viewModelScope.launch(Dispatchers.IO) {

            val bitmap1 = context.getDrawable(R.drawable.ecatsobor)!!.toBitmap()
            val bitmap2 = context.getDrawable(R.drawable.alexnevsk)!!.toBitmap()
            val bitmap3 = context.getDrawable(R.drawable.easyphot)!!.toBitmap()
            val bitmap4 = context.getDrawable(R.drawable.alice)!!.toBitmap()
            val bitmap5 = context.getDrawable(R.drawable.krasnodar)!!.toBitmap()
            val bitmap6 = context.getDrawable(R.drawable.moskovks)!!.toBitmap()
            val bitmap7 = context.getDrawable(R.drawable.whiteia)!!.toBitmap()
            val stream1 = ByteArrayOutputStream()
            val stream2 = ByteArrayOutputStream()
            val stream3 = ByteArrayOutputStream()
            val stream4 = ByteArrayOutputStream()
            val stream5 = ByteArrayOutputStream()
            val stream6 = ByteArrayOutputStream()
            val stream7 = ByteArrayOutputStream()
            bitmap1.compress(Bitmap.CompressFormat.PNG, 90, stream1)
            bitmap2.compress(Bitmap.CompressFormat.PNG, 90, stream2)
            bitmap3.compress(Bitmap.CompressFormat.PNG, 90, stream3)
            bitmap4.compress(Bitmap.CompressFormat.PNG, 90, stream4)
            bitmap5.compress(Bitmap.CompressFormat.PNG, 90, stream5)
            bitmap6.compress(Bitmap.CompressFormat.PNG, 90, stream6)
            bitmap7.compress(Bitmap.CompressFormat.PNG, 90, stream7)
            val ecat = stream1.toByteArray()
            val alex = stream2.toByteArray()
            val easy = stream3.toByteArray()
            val alice = stream4.toByteArray()
            val krasnodar = stream5.toByteArray()
            val moskovs = stream6.toByteArray()
            val whiteia = stream7.toByteArray()
            var fos: FileOutputStream? = null
            fos= context.openFileOutput("ecatsobor.png", MODE_PRIVATE)
            fos.write(ecat)
            fos.close()
            fos= context.openFileOutput("alexnevsk.png", MODE_PRIVATE)
            fos.write(alex)
            fos.close()
            fos= context.openFileOutput("easyphot.png", MODE_PRIVATE)
            fos.write(easy)
            fos.close()
            fos= context.openFileOutput("alice.png", MODE_PRIVATE)
            fos.write(alice)
            fos.close()
            fos= context.openFileOutput("krasnodar.png", MODE_PRIVATE)
            fos.write(krasnodar)
            fos.close()
            fos= context.openFileOutput("moskovks.png", MODE_PRIVATE)
            fos.write(moskovs)
            fos.close()
            fos= context.openFileOutput("whiteia.png", MODE_PRIVATE)
            fos.write(whiteia)
            fos.close()

            try {
                markerManager.markerRepository.insertList(
                    listOf(
                        MarkerPoint(
                            id = 0,
                            name = "Екатерина собор",
                            latitude = 45.02052,
                            longitude = 38.97454,
                            img = "ecatsobor",
                            description = "Свято-Екатерининский кафедральный собор ",
                            startWork = 8 * 60 * 60 * 1000L,
                            endWork = 18 * 60 * 60 * 1000L,
                            rating = 5.0f,
                            type = attraction, price = 0),
                        MarkerPoint(
                            id = 1,
                            name = "Войсковой собор святого Благоверного Князя Александра Невского",
                            latitude = 45.01436,
                            longitude = 38.96696,
                            img = "alexnevsk",
                            description = "Войсковой собор святого Благоверного Князя Александра Невского",
                            startWork = 8 * 60 * 60 * 1000L,
                            endWork = 18 * 60 * 60 * 1000L,
                            rating = 5.0f,
                            type = attraction,
                            price = 0),
                        MarkerPoint(id = 2, name = "Стадион “Краснодар”",
                            latitude = 45.04442,
                            longitude = 39.0293,
                            img = "krasnodar",
                            description = "Стадион “Краснодар” ",
                            startWork = 8 * 60 * 60 * 1000L,
                            endWork = 18 * 60 * 60 * 1000L,
                            rating = 5.0f,
                            type = attraction, price = 0),
                        MarkerPoint(id = 3,
                            name = "EasyPhoto.Studio",
                            latitude = 45.03215,
                            longitude = 39.02482,
                            img = "easyphot",
                            description = "EasyPhoto.Studio ",
                            startWork = 8 * 60 * 60 * 1000L,
                            endWork = 18 * 60 * 60 * 1000L,
                            rating = 5.0f,
                            type = photoZone,
                            price = 0),
                        MarkerPoint(id = 4,
                            name = "Alice",
                            latitude = 45.06229,
                            longitude = 38.99264,
                            img = "alice",
                            description = "Alice",
                            startWork = 8 * 60 * 60 * 1000L,
                            endWork = 18 * 60 * 60 * 1000L,
                            rating = 5.0f,
                            type = photoZone,
                            price = 0),
                        MarkerPoint(id = 5, name = "Белый осел",
                            latitude = 45.06326,
                            longitude = 38.99113,
                            img = "whiteia",
                            description = "Белый осел ",
                            startWork = 8 * 60 * 60 * 1000L,
                            endWork = 15 * 60 * 60 * 1000L,
                            rating = 5.0f,
                            type = photoZone, price = 0),
                        MarkerPoint(id = 6, name = "Аллея на Московской", latitude = 45.06797,
                            longitude = 39.01165, img = "moskovks",
                            description = "Аллея на Московской ", startWork = 8 * 60 * 60 * 1000L,
                            endWork = 18 * 60 * 60 * 1000L, rating = 5.0f,
                            type = userPoint, price = 0)
                    )

                )
            } catch (throwable: SQLiteException) {
                _sharedStateFlowError.emit(ErrorApp.FailureDataBase(throwable.toString()))
            }
        }
    }

    private fun getAttraction() {
        viewModelScope.launch(Dispatchers.IO) {
            markerManager.markerRepository.getAllMarkersByType(attraction).collect {
                when (it) {
                    is ResponseDataBase.Empty -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Empty)
                    }
                    is ResponseDataBase.Success -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Success(mapOf(Pair(attraction,
                            it.value))))
                    }
                    is ResponseDataBase.Failure -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Failure(it.errorBody))
                    }
                }
            }
        }
    }

    fun clickOnMarker(marker: MarkerPoint) {
        viewModelScope.launch(Dispatchers.IO) {
            markerManager.markerFlow.emit(GoToInfo.FromMain(marker))
        }
    }


    private fun getUserPoint() {
        viewModelScope.launch(Dispatchers.IO) {
            println("getUserPoint      : I'm working in thread ${Thread.currentThread().name}")
            markerManager.markerRepository.getAllMarkersByType(userPoint).collect {
                when (it) {
                    is ResponseDataBase.Empty -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Empty)
                    }
                    is ResponseDataBase.Success -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Success(mapOf(Pair(userPoint,
                            it.value))))
                    }
                    is ResponseDataBase.Failure -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Failure(it.errorBody))
                    }
                }
            }
        }
    }

    private fun getPhotoZone() {
        viewModelScope.launch(Dispatchers.IO) {
            println("getPhotoZone      : I'm working in thread ${Thread.currentThread().name}")
            markerManager.markerRepository.getAllMarkersByType(photoZone).collect {
                when (it) {
                    is ResponseDataBase.Empty -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Empty)
                    }
                    is ResponseDataBase.Success -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Success(mapOf(Pair(photoZone,
                            it.value))))
                    }
                    is ResponseDataBase.Failure -> {
                        _responseDataBaseStateFlow.emit(DataToMain.Failure(it.errorBody))
                    }
                }
            }
        }
    }

    private fun clearDataFromMap() {
        viewModelScope.launch {
            println("clearDataFromMap      : I'm working in thread ${Thread.currentThread().name}")
            _responseDataBaseStateFlow.emit(DataToMain.Clear)
        }
    }

    //  val _buttonStateFlow = markerManager.locationFlow
    //val  buttonStateFlow:StateFlow<ResponseHome>
    private val _locationFlow: MutableStateFlow<ResponseDataBase<UserLocation>> =
        MutableStateFlow(ResponseDataBase.Empty)
    val locationFlow: StateFlow<ResponseDataBase<UserLocation>> = _locationFlow

    private fun getLastLocation() = viewModelScope.launch {
        userLocationRepository.getLastLocation().collect {
            when (it) {
                is ResponseDataBase.SuccessNotList -> _locationFlow.emit(it)
                is ResponseDataBase.Failure -> _locationFlow.emit(it)
                is ResponseDataBase.Empty -> _locationFlow.emit(it)
                else -> {}
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        Log.d("onCleared", "onCleared onCleared")

    }
}


class FactoryMainView @Inject constructor(
    private val userPreferences: UserPreferences,
    private val gpsRepository: GpsRepository,
    private val markerManager: MarkerManager,
    private val userLocationRepository: UserLocationRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(userPreferences,
            gpsRepository,
            markerManager,
            userLocationRepository) as T
    }
}