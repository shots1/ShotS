package com.example.mainactivity.fragments.infoFragment

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.core_api.dto.MarkerPoint
import com.example.mainactivity.MarkerManager
import com.example.utils.GoToInfo
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class InfoViewModel(
    private val markerManager: MarkerManager,
) : ViewModel() {

    private val _sharedFlowFromMain = MutableSharedFlow<MarkerPoint>(replay = 1,
        extraBufferCapacity = 0,
        onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val sharedFlowFromMain = markerManager.markerFlow.asSharedFlow()

    var markerTouch :Boolean = false
    override fun onCleared() {
        Log.d("InfoViewModel", "onCleard")
        super.onCleared()
    }
    fun emitTomarkerFlow(){
        viewModelScope.launch {
            markerManager.markerFlow.emit(GoToInfo.FromInfo)
        }
    }
}

class InfoFactory @Inject constructor(
    private val markerManager: MarkerManager,

    ) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return InfoViewModel( markerManager) as T
    }
}