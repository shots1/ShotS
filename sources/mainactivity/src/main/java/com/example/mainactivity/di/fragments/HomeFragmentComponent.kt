package com.example.mainactivity.di.fragments


import com.example.core_api.HomeScope
import com.example.mainactivity.di.MainActvitityComponent
import com.example.mainactivity.fragments.home.HomeFragment
import dagger.Component
@HomeScope
@Component(
    dependencies = [MainActvitityComponent::class],
)
interface HomeFragmentComponent {
    fun inject(fragment: HomeFragment)
    @Component.Factory
    interface Factory {
        fun create(applicationComponent: MainActvitityComponent): HomeFragmentComponent
    }
}