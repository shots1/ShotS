package com.example.mainactivity.di

import android.content.Context
import com.example.core_api.MainActivityScope
import com.example.core_api.mediator.ProvidersFacade
import com.example.mainactivity.MainActivity
import com.example.mainactivity.MarkerManager
import com.example.repo.di.RepoModule
import com.example.repo.filter.FiltersRepository
import com.example.repo.gps.GpsDataSource
import com.example.repo.hardware.GpsRepository
import com.example.repo.local.UserPreferences
import com.example.repo.location.UserLocationRepository
import com.example.repo.markers.MarkerRepository
import dagger.Component
import dagger.Module
import dagger.Provides


@Module
class MainModule {
    @MainActivityScope
    @Provides
    fun provideMarkerManager(
        markerRepository: MarkerRepository,
        filtersRepository: FiltersRepository,
    ): MarkerManager {
        return MarkerManager(markerRepository, filtersRepository)
    }
}

@Component(
    dependencies = [ProvidersFacade::class],
    modules = [MainModule::class,RepoModule::class]
)
@MainActivityScope
interface MainActvitityComponent {

    fun provideMarkerManager(): MarkerManager

    fun provideGpsRepo(): GpsRepository

    fun provideGpsSource(): GpsDataSource

    @Suppress("FunctionName")
    fun bindsMarkserRepository_to_MarkerRepositoryImpl(): MarkerRepository

    @Suppress("FunctionName")
    fun bindsFilterRepository_to_FilterRepositoryImpl(): FiltersRepository

    fun getApplication(): Context

    fun inject(activity: MainActivity)

    @Suppress("FunctionName")
    fun bindsUserLocationRepository_to_UserLocationRepositoryImpl(): UserLocationRepository

    @Suppress("FunctionName")
    fun bindsUserPref_to_UserPrefImpl(): UserPreferences

    fun providersFacade(): ProvidersFacade

    companion object {

        fun create(providersFacade: ProvidersFacade): MainActvitityComponent {
            return DaggerMainActvitityComponent.builder().providersFacade(providersFacade).build()
        }
    }
}