package com.example.mainactivity.network


import com.example.repo.local.UserPreferences
import retrofit2.Retrofit
import javax.inject.Inject

private const val BASE_URL = "https://auth.tragltech.com/otus/api/"

class RemoteDataSource @Inject constructor(
    private val preferences: UserPreferences,
    private val retrofit: Retrofit
) {

    fun <Api> buildApi(
        api: Class<Api>,
    ): Api {
        val authenticator = TokenAuthenticator(buildTokenApi(), preferences)
        return retrofit.create(api)
    }

    private fun buildTokenApi(): TokenRefreshApi {
        return retrofit.create(TokenRefreshApi::class.java)
    }


}