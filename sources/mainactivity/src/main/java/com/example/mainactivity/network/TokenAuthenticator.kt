package com.example.mainactivity.network

import com.example.core_api.dto.TokenResponse
import com.example.mainactivity.repo.BaseRepository
import com.example.repo.local.UserPreferences
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject
import com.example.utils.Response as DataResponse

class TokenAuthenticator @Inject constructor(
    private val tokenApi: TokenRefreshApi,
    private val preferences: UserPreferences
) : Authenticator, BaseRepository(tokenApi) {

    override fun authenticate(route: Route?, response: Response): Request? {
        return runBlocking {
            when (val tokenResponse = getUpdatedToken()) {
                is DataResponse.Success -> {
                    preferences.saveAccessTokens(
                        tokenResponse.value.access_token,
                        tokenResponse.value.refresh_token
                    )
                    response.request.newBuilder()
                        .header("Authorization", "Bearer ${tokenResponse.value.access_token}")
                        .build()
                }
                else -> null
            }
        }
    }

    private suspend fun getUpdatedToken(): DataResponse<TokenResponse> {
        val refreshToken = preferences.refreshToken.first()
        return safeApiCall {
            tokenApi.refreshAccessToken(refreshToken)
        }
    }
}