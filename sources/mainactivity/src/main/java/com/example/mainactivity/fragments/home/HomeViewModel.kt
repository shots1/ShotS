package com.example.mainactivity.fragments.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.mainactivity.MarkerManager
import com.example.utils.ResponseHome
import com.example.utils.ResponseSplash
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel(
    private val markerManager: MarkerManager
) : ViewModel() {

    val markerSharedFlow = markerManager.markerFlow.asSharedFlow()

    private val _buttonsState:MutableStateFlow<ResponseHome> =  MutableStateFlow(
        ResponseHome.Loading)
    val buttonStateFlow :StateFlow<ResponseHome> = _buttonsState.asStateFlow()

    var locationFlow: StateFlow<ResponseSplash<Any?>>? = null

    var markerTouch :Boolean = false

    fun attractionButtonClick(){
        viewModelScope.launch {
            if (_buttonsState.value == ResponseHome.Attraction) {
                _buttonsState.emit(ResponseHome.Loading)
                markerManager.locationFlow.emit(ResponseHome.Loading)
            } else {
                _buttonsState.emit(ResponseHome.Attraction)
                markerManager.locationFlow.emit(ResponseHome.Attraction)
                Log.d("attractionButtonClick", "Attraction")
            }
        }
    }

    fun photoZoneButtonClick(){
        viewModelScope.launch {
            if (_buttonsState.value == ResponseHome.PhotoZone) {
                _buttonsState.emit(ResponseHome.Loading)
                markerManager.locationFlow.emit(ResponseHome.Loading)
            } else {
                _buttonsState.emit(ResponseHome.PhotoZone)
                markerManager.locationFlow.emit(ResponseHome.PhotoZone)
                Log.d("attractionButtonClick", "Attraction")
            }
        }
    }

     fun userPointButtonClick(){
        viewModelScope.launch {
            if (_buttonsState.value == ResponseHome.UserPoint) {
                _buttonsState.emit(ResponseHome.Loading)
                markerManager.locationFlow.emit(ResponseHome.Loading)
            } else {
                _buttonsState.emit(ResponseHome.UserPoint)
                markerManager.locationFlow.emit(ResponseHome.UserPoint)
                Log.d("attractionButtonClick", "Attraction")
            }
        }
    }
    override fun onCleared() {
        super.onCleared()
        Log.d("onCleared", "onCleared onCleared")
        locationFlow = null
    }
}


class FactoryHomeView @Inject constructor(
    private val markerManager: MarkerManager
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeViewModel(markerManager) as T
    }
}