package com.example.mainactivity.network

import com.example.core_api.dto.LoginResponse
import retrofit2.http.GET

interface UserApi : BaseApi {

    @GET("user")
    suspend fun getUser(): LoginResponse
}