package com.example.mainactivity.di


import com.example.core_api.mediator.ProvidersFacade
import com.example.mainactivity.splashFragment.SplashActivity

import com.example.repo.di.RepoModule
import dagger.Component


@Component(
    dependencies = [ProvidersFacade::class],
    modules = [RepoModule::class]
)
interface SplashActivitycomponent {
    fun inject(acitivity: SplashActivity)
    companion object {

        fun create(providersFacade: ProvidersFacade): SplashActivitycomponent {
            return DaggerSplashActivitycomponent.builder().providersFacade(providersFacade).build()
        }
    }
}
