package com.example.mainactivity.fragments.filter.listFilter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.mainactivity.MainActivity
import com.example.mainactivity.OnBackPressedFrament
import com.example.mainactivity.R
import com.example.mainactivity.databinding.FragmentListFilterBinding
import com.example.mainactivity.di.fragments.DaggerListFilterFragmentComponent
import com.example.mainactivity.di.fragments.ListFilterFragmentComponent
import com.example.utils.ResponseDataBase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


class ListFilterFragment : Fragment(R.layout.fragment_list_filter), CoroutineScope,
    OnBackPressedFrament {
    private val job: Job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO
    private lateinit var navController: NavController
    private val binding: FragmentListFilterBinding by viewBinding()

    lateinit var listFilterComponent: ListFilterFragmentComponent
    private lateinit var adapter: ListFilterAdapter
private lateinit  var mContext:Context
    @Inject
    lateinit var factory: FactoryListFilterView
    private val viewModelFilter by viewModels<ListFilterViewModel> { factory }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        listFilterComponent = DaggerListFilterFragmentComponent.factory()
            .create((requireActivity() as MainActivity).activityComponent)
        listFilterComponent.inject(this)
        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        navController = findNavController()
        initListeners()
        initView()
    }

    fun initView(){
        binding.customText1.setPaintBlackFull()
        binding.customText2.setPaintBlackStroke()
        binding.customText1.setlistener{
            navController.popBackStack()
        }
        binding.customText2.setValues(mContext.getString(R.string.filter_list_delete))
        binding.customText2.setlistener {viewModelFilter.clearFilter()}
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("ListFilterFragment","onDestroy")
    }
    private fun initAdapter(){
        adapter = ListFilterAdapter {
            viewModelFilter.changeFilter(it)
        }
        binding.recyclerView.adapter = adapter
        viewModelFilter.getAllFilters()

        //adapter.update(),diffUtil)

    }
    private fun initListeners (){
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModelFilter.sharedStateFlowDataBase.collect {
                when(it){
                    is ResponseDataBase.SuccessNotList->{
                        adapter.setFilter(it.value)
                    }
                    is ResponseDataBase.Failure ->{
                        Toast.makeText(context,
                            getString(R.string.common_error) + " " + it.errorBody,
                            Toast.LENGTH_LONG)
                            .show()
                    }
                    is ResponseDataBase.Empty ->{
                        adapter.clear()
                    }
                }
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModelFilter.stateFlowSizeMarkers.collect { binding.customText1.setValues(mContext.getString(R.string.filter_listview_result_size,it)) } }
    }


    override fun onBack(): Boolean {
       return false
    }
}