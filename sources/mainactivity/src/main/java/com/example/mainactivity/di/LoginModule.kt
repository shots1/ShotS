package com.example.mainactivity.di

import com.example.mainactivity.network.AuthApi
import com.example.mainactivity.network.RemoteDataSource
import com.example.mainactivity.network.UserApi

import dagger.Module
import dagger.Provides

@Module
class LoginModule {


    @Provides
    fun provideAuthApi(
        remoteDataSource: RemoteDataSource,
    ): AuthApi {
        return remoteDataSource.buildApi(AuthApi::class.java)
    }

    @Provides
    fun provideUserApi(
        remoteDataSource: RemoteDataSource,
    ): UserApi {
        return remoteDataSource.buildApi(UserApi::class.java)
    }

}