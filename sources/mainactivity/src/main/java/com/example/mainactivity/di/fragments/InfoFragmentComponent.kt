package com.example.mainactivity.di.fragments



import com.example.core_api.HomeScope
import com.example.mainactivity.di.MainActvitityComponent
import com.example.mainactivity.fragments.infoFragment.InfoFragment
import dagger.Component

@HomeScope
@Component(
    dependencies = [MainActvitityComponent::class],
)
interface InfoFragmentComponent {
    fun inject(fragment: InfoFragment)
    @Component.Factory
    interface Factory {
        fun create(applicationComponent: MainActvitityComponent): InfoFragmentComponent
    }
}