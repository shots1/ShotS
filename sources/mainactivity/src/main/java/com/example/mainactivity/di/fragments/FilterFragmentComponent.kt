package com.example.mainactivity.di.fragments



import com.example.core_api.HomeScope
import com.example.mainactivity.di.MainActvitityComponent
import com.example.mainactivity.fragments.filter.FilterFragment
import dagger.Component
@HomeScope
@Component(
    dependencies = [MainActvitityComponent::class],
)
interface FilterFragmentComponent {
    fun inject(fragment: FilterFragment)
    @Component.Factory
    interface Factory {
        fun create(applicationComponent: MainActvitityComponent): FilterFragmentComponent
    }
}