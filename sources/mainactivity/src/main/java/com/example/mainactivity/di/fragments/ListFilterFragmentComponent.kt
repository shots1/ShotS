package com.example.mainactivity.di.fragments


import com.example.core_api.HomeScope
import com.example.mainactivity.di.MainActvitityComponent
import com.example.mainactivity.fragments.filter.listFilter.ListFilterFragment

import dagger.Component

@HomeScope
@Component(
    dependencies = [MainActvitityComponent::class],
)
interface ListFilterFragmentComponent {
    fun inject(fragment: ListFilterFragment)
    @Component.Factory
    interface Factory {
        fun create(applicationComponent: MainActvitityComponent): ListFilterFragmentComponent
    }
}