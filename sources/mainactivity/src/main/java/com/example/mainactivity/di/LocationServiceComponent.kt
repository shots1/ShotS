package com.example.mainactivity.di


import com.example.core_api.mediator.ProvidersFacade
import com.example.mainactivity.services.LocationService
import com.example.repo.di.RepoModule
import dagger.Component


@Component(
    dependencies = [ProvidersFacade::class],
    modules = [RepoModule::class]
)
interface LocationServiceComponent {
    fun inject(fragment: LocationService)
    companion object {
        fun create(providersFacade: ProvidersFacade): LocationServiceComponent {
            return DaggerLocationServiceComponent.builder().providersFacade(providersFacade).build()
        }
    }
}