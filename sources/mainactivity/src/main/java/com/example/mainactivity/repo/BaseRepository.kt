package com.example.mainactivity.repo

import com.example.mainactivity.network.BaseApi

abstract class BaseRepository(
    private val api: BaseApi,
) : SafeApiCall {

    suspend fun logout() = safeApiCall {
        api.logout()
    }
}
