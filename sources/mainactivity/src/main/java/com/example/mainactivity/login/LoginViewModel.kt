package com.example.mainactivity.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.core_api.dto.LoginResponse
import com.example.mainactivity.repo.AuthRepository
import com.example.repo.local.UserPreferences
import com.example.utils.Response
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel(
    private val userPreferences: UserPreferences,
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _loginStateFlow: MutableStateFlow<Response<LoginResponse?>> = MutableStateFlow(
        Response.Empty)
    val loginStateFlow: StateFlow<Response<LoginResponse?>> = _loginStateFlow.asStateFlow()

    fun login(
        email: String,
        password: String
    ) = viewModelScope.launch {
        _loginStateFlow.value = Response.Loading
        _loginStateFlow.value = authRepository.login(email, password)
    }

    suspend fun saveAccessTokens(accessToken: String, refreshToken: String) {
        userPreferences.saveAccessTokens(accessToken, refreshToken)
    }
}

class FactoryLogin @Inject constructor(
    private val userPreferences: UserPreferences,
    private val authRepository: AuthRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LoginViewModel(userPreferences,authRepository) as T
    }
}