package com.example.mainactivity.fragments.filter

import android.util.Log
import androidx.lifecycle.*
import com.example.mainactivity.MarkerManager
import com.example.repo.filter.FiltersRepository
import com.example.repo.local.UserPreferences
import com.example.utils.ErrorApp
import com.example.utils.ResponseDataBase
import com.example.utils.ResponseHome
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class FilterViewModel(
    private val userPreferences: UserPreferences,
    private val filtersRepository: FiltersRepository,
    private val markerManager: MarkerManager,
) : ViewModel() {
    private val _sharedStateFlowError = MutableSharedFlow<ErrorApp<Any?>>(replay = 0,
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val sharedStateFlowError = _sharedStateFlowError.asSharedFlow()
    private val coroutineException = CoroutineExceptionHandler { coroutineContext, throwable ->
        viewModelScope.launch(Dispatchers.Main) {
            _sharedStateFlowError.emit(ErrorApp.FailureUnknown(throwable.toString()))
            Log.d("FilterViewModel", throwable.toString())
        }
    } + CoroutineName("FilterViewModel")

    private val _sharedStateFlowDataBase = MutableSharedFlow<ResponseDataBase<Any?>>(replay = 1,
        extraBufferCapacity = 0,
        onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val sharedStateFlowDataBase = _sharedStateFlowDataBase.asSharedFlow()

    private val _sharedStateFlowString = MutableSharedFlow<String>(replay = 1,
        extraBufferCapacity = 0,
        onBufferOverflow = BufferOverflow.DROP_OLDEST)
    val sharedStateFlowString = _sharedStateFlowString.asSharedFlow()

    val markerSharedFlow = markerManager.markerFlow.asSharedFlow()
    var markerTouch: Boolean = false
    private val _text = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }
    val text: LiveData<String> = _text


    init {
        getAllMarkers()
    }

    private fun getAllMarkers() {
        viewModelScope.launch(Dispatchers.IO + coroutineException) {
            markerManager.getAllMarkersWithFilter().collect { markerList ->
                when (markerList) {
                    ResponseDataBase.Empty -> {
                        _sharedStateFlowDataBase.emit(ResponseDataBase.Empty)
                        markerManager.locationFlow.emit(ResponseHome.Loading)
                        Log.d("getAllMarkers", "Empty")
                    }
                    is ResponseDataBase.Success -> {
                        Log.d("getAllMarkers", "Success")
                        var markerListThis = markerList.value
                        _sharedStateFlowDataBase.emit(ResponseDataBase.Success(
                            markerListThis))
                        markerManager.markerPointFromFilters.emit(markerListThis)
                    }
                    is ResponseDataBase.Failure -> {
                        _sharedStateFlowDataBase.emit(ResponseDataBase.Failure(markerList.errorBody))
                        markerManager.locationFlow.emit(ResponseHome.Loading)

                    }
                    else -> {}
                }
            }
        }
    }

    fun serchStateChanged(string: String) {
        viewModelScope.launch {
            _sharedStateFlowString.emit(string)
        }
    }

    fun clearFilters() {
        viewModelScope.launch(Dispatchers.IO + coroutineException) {
            filtersRepository.delete()
            markerManager.locationFlow.emit(ResponseHome.Loading)
        }
    }

    override fun onCleared() {
        Log.d("FilterViewModel", "onCleard")
        super.onCleared()
    }
}

class FactoryFilterView @Inject constructor(
    private val userPreferences: UserPreferences,
    private val filtersRepository: FiltersRepository,
    private val markerManager: MarkerManager,

    ) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FilterViewModel(userPreferences, filtersRepository, markerManager) as T
    }
}