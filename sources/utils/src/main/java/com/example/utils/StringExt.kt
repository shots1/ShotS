package com.example.utils

fun Any?.toStringOrEmpty() = this?.toString().orEmpty()