package com.example.utils

const val photoZone = 1
const val attraction = 2
const val userPoint = 3
const val paid = 1
const val free = 2
const val SortBy = 0
const val Type = 1
const val Rating = 2
const val Availability = 3


const val empty = 0
const val bestNearBy = 1
const val sortByRating = 2
const val open = 1
const val closed = 2
