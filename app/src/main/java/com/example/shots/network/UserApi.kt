package com.example.shots.network

import com.example.core_api.dto.LoginResponse
import com.example.mainactivity.network.BaseApi
import retrofit2.http.GET

interface UserApi : BaseApi {

    @GET("user")
    suspend fun getUser(): LoginResponse
}