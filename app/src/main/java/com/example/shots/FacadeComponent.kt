package com.example.shots

import android.app.Application
import com.example.core_api.database.DatabaseProvider
import com.example.core_api.mediator.AppProvider
import com.example.core_api.mediator.ProvidersFacade
import com.example.core_api.network.NetworkProvider
import com.example.coredatabase.CoreProvidersFactory
import dagger.Component

@Component(
    dependencies = [AppProvider::class, DatabaseProvider::class,NetworkProvider::class],
    modules = [MediatorsBindings::class]
)

interface FacadeComponent : ProvidersFacade {

    companion object {

        fun init(application: Application): FacadeComponent =
            DaggerFacadeComponent.builder()
                .appProvider(AppComponent.create(application))
                .databaseProvider(CoreProvidersFactory.createDatabaseBuilder(AppComponent.create(application)))
                .networkProvider(CoreProvidersFactory.createRetrofit())
                .build()
    }


    fun inject(app: App)
}